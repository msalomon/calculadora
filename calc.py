# Ejercicio 16.8.-
# Programa que funcione como una calculadora
# que suma y resya

# Funcion Suma
def sumar(a, b):
    suma = a + b
    return suma

# Funcion Resta
def restar(a, b):
    resta = a - b
    return resta


print("1 + 2 = ", sumar(1,2))
print("3 + 4 = ", sumar(3,4))
print("5 - 6 = ", restar(5,6))
print("7 - 8 = ", restar(7,8))